package com.example.ordersService.dto;


import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class CheckProductResponse {
    private String productId;
    private Boolean isInStock;

}
