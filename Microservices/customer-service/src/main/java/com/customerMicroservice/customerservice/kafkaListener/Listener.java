package com.customerMicroservice.customerservice.kafkaListener;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;


@Component
public class Listener {


    @KafkaListener(topics = "test")
    public void handleNotification(OrderPlacedEvent orderPlacedEvent) {
        System.out.println(orderPlacedEvent);
    }
}
