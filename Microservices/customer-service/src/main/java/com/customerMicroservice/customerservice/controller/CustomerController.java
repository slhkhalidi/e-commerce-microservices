package com.customerMicroservice.customerservice.controller;
import com.customerMicroservice.customerservice.dto.CustomerResponse;
import com.customerMicroservice.customerservice.model.Customer;
import com.customerMicroservice.customerservice.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    /**
     * Create
     */
    @PostMapping("/create")
    public ResponseEntity<String> createCustomer(@RequestBody Customer customer)  {
         return this.customerService.createCustomer(customer);
    }


    /**
     * Get All
     */
    @GetMapping("/")
    public List<Customer> getAllCustomers() {
        return this.customerService.getAllCustomers();
    }



    /**
     * Get By ID
     */
    @GetMapping("/findById")
    public ResponseEntity<Customer> getCustomerById(@RequestParam String id) {
        return this.customerService.getCustomerById(id);
    }





    /**
     * Update
     */
    @PutMapping("/update")
    public ResponseEntity<CustomerResponse> updateCustomer(@RequestBody Customer customer) {
        return this.customerService.updateCustomer(customer);
    }




    /**
     * Delete
     */
    @DeleteMapping("/delete")
    public ResponseEntity<String> deleteCustomer(@RequestParam String id) {
        return this.customerService.deleteCustomerById(id);
    }


}
