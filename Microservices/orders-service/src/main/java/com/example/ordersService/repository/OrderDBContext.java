package com.example.ordersService.repository;

import com.example.ordersService.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderDBContext extends JpaRepository<Order, Long> {
}
