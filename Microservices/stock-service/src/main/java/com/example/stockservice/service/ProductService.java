package com.example.stockservice.service;


import com.example.stockservice.dto.CheckProductResponse;
import com.example.stockservice.model.ProductModel;
import com.example.stockservice.repo.ProductRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@Slf4j
public class ProductService {

    private ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    /**
     * Get ALl
     */
    public ResponseEntity<List<ProductModel>> getAll() {
        List<ProductModel> myList =  productRepository.findAll();
        return new ResponseEntity<>(myList, HttpStatus.OK);
    }

    /**
     * Get All Products By Categorie
     * @param categorie
     * @return
     */
    public ResponseEntity<List<ProductModel>> getAllProductsByCategorie(String categorie) {
        List<ProductModel> myList =  productRepository.findAllByCategory(categorie);
        return  new ResponseEntity<>(myList, HttpStatus.OK);
    }


    /**
     * Create a Product
     * @param productModel
     * @return
     */
    public ResponseEntity<String> createProduct(ProductModel productModel) {
        try {
            productRepository.insert(productModel);
            return new ResponseEntity<>("Product is created", HttpStatus.OK);

        } catch (Exception e) {
           return new ResponseEntity<>("Product Failed to create", HttpStatus.BAD_REQUEST);
        }
    }


    /**
     * Check are the Product in Stock or not
     */
    public ResponseEntity<List<CheckProductResponse>> isInStock(String productIds) throws InterruptedException {
        /** log.info("Wait is started");
        Thread.sleep(10000);
        log.info("Wait is ended"); **/
        String[] productsIds = productIds.split(",");
        List<ProductModel> myList = Arrays.stream(productsIds)
                .map(id -> productRepository.findById(id).get()).toList();
        List<CheckProductResponse> myList2 = myList.stream()
                    .map(product ->
                        CheckProductResponse.builder()
                                .productId(product.getProductId())
                                .isInStock(product.getInStock())
                                .build()
                    ).collect(Collectors.toList());
        return new ResponseEntity<>(myList2, HttpStatus.OK);

    }


}
