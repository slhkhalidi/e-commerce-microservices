package com.example.ordersService.service;


import com.example.ordersService.dto.CheckProductResponse;
import com.example.ordersService.event.OrderPlacedEvent;
import com.example.ordersService.model.Order;
import com.example.ordersService.model.OrderItem;
import com.example.ordersService.repository.OrderDBContext;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.CompletableFuture;

@Service
@RequiredArgsConstructor
public class OrderService {


    private final OrderDBContext orderDBContext;

    private final WebClient.Builder webClientBuilder;

    private final KafkaTemplate<String, OrderPlacedEvent> kafkaTemplate;





    /**
     * Create Order
     */
    public ResponseEntity<String> createOrder(Order order) {
        /**
         *  Infos: .block() is to make synchronous Request.
         *  1- Collect the ProductIds from the Order in a List.
         *  2- Send Request to Product Service and check which Product is available.
         *  3- return the response.
         * **/
        List<String> productIdsList = order.getOrderItems().stream().map(OrderItem::getProductId).toList();
        String myUri = "http://stock-service/api/v1/product/getProducts/"+String.join(",",productIdsList);
        CheckProductResponse myList[] = webClientBuilder.build().get()
                                   .uri(myUri)
                                   .retrieve()
                                   .bodyToMono(CheckProductResponse[].class)
                                   .block();
       Boolean isAllInStock = Arrays.stream(myList).allMatch(CheckProductResponse::getIsInStock);
       if(isAllInStock) {
           orderDBContext.save(order);
           // Send Asynchronous Message to Customer-Service through Kafka //
           kafkaTemplate.send("test",new OrderPlacedEvent(order.getOrderId()));
           return new ResponseEntity<>("Order is successfully created", HttpStatus.OK);
       } else {
           throw new IllegalArgumentException("One of the Products are not in Stock");
       }
    }


    /**
     * Update Order
     */
    public ResponseEntity<Order> updateOrder(Order order) {
        Order newOrder = null;
        try {
              newOrder = orderDBContext.save(order);
            return new ResponseEntity<>(newOrder, HttpStatus.OK);
        } catch(NoSuchElementException e) {
            return new ResponseEntity<>(newOrder, HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Delete Order
     */
    public ResponseEntity<String> deleteOrder(Long orderId) {
        Order token = null;
        try {
            orderDBContext.deleteById(orderId);
            return new ResponseEntity<>("The Order with orderId: "+orderId+" was deleted.", HttpStatus.OK);
        } catch(EmptyResultDataAccessException e) {
            return new ResponseEntity<>("The Order with orderId: "+orderId+" was not found.", HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Get all Orders
     */
    public ResponseEntity<List<Order>> getOrders() {
        List<Order> myOrders = orderDBContext.findAll();
        return new ResponseEntity<>(myOrders, HttpStatus.OK);
    }

    /**
     * Get one Order By ID
     */
    public ResponseEntity<Order> getOrderById(Long orderId) {
        Order token = null;
        try {
            token = orderDBContext.findById(orderId).get();
            return new ResponseEntity<>(token, HttpStatus.OK);
        } catch(NoSuchElementException e) {
            return new ResponseEntity<>(token, HttpStatus.NOT_FOUND);
        }
    }
}
