package com.customerMicroservice.customerservice.repository;

import com.customerMicroservice.customerservice.model.Customer;
import org.springframework.data.domain.Example;
import org.springframework.data.mongodb.repository.ExistsQuery;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Optional;

public interface CustomerRepo extends MongoRepository<Customer, String> {

}
