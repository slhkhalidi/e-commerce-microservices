package com.customerMicroservice.customerservice.service;


import com.customerMicroservice.customerservice.dto.CustomerResponse;
import com.customerMicroservice.customerservice.model.Customer;
import com.customerMicroservice.customerservice.repository.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class CustomerService {

    @Autowired
    private  CustomerRepo customerRepository;

    public ResponseEntity<String> createCustomer(Customer customer) {
       try {
           this.customerRepository.insert(customer);
           return new ResponseEntity<>("Yes", HttpStatus.CREATED);
       } catch (Exception e) {
           return new ResponseEntity<>("No", HttpStatus.BAD_REQUEST);
       }
    }

    public List<Customer> getAllCustomers() {
      List<Customer> myList = this.customerRepository.findAll();
      return myList;
      // return new ResponseEntity<>(myList, HttpStatus.OK);
    }


    public ResponseEntity<Customer> getCustomerById(String id) {
        Customer token  = null;
        try {
            token = this.customerRepository.findById(id).get();
            return new ResponseEntity<>(token, HttpStatus.OK);
        } catch(NoSuchElementException e) {
            return new ResponseEntity<>(token, HttpStatus.NOT_FOUND);
        }
    }


    public ResponseEntity<CustomerResponse> updateCustomer(Customer customer) {
        Customer token = null;
        try {
            token =  customerRepository.save(customer);
            CustomerResponse response = new CustomerResponse(customer, "Successfully updated");
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            CustomerResponse response = new CustomerResponse(token, "No Such Element with this ID");
            return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
        }
    }


    public ResponseEntity<String> deleteCustomerById(String id) {
    try {
        Customer checker = this.customerRepository.findById(id).get();
        this.customerRepository.deleteById(id);
        return new ResponseEntity<>("User is deleted", HttpStatus.OK);
     } catch (NoSuchElementException ex) {
        return new ResponseEntity<>("User was not found", HttpStatus.NOT_FOUND);
     }
    }




}
