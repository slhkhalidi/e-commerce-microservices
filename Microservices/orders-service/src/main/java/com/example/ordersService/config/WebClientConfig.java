package com.example.ordersService.config;


import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebClientConfig {

    /**
     * We use @Bean to say that this instance should be managed by Spring Container
     * The Bean is an instance of a Class managed by Spring container
     * The LoadBalancer give the Webclient the permission to check which Instance to connect with. (Service Discovery)
     */
    @Bean
    @LoadBalanced
    public WebClient.Builder webClient(){
        return WebClient.builder();
    }


}
