package com.customerMicroservice.customerservice;


import com.customerMicroservice.customerservice.model.Customer;
import com.customerMicroservice.customerservice.repository.CustomerRepo;
import com.customerMicroservice.customerservice.service.CustomerService;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {

    /**
     * @Mock: Create Object
     * @InjectMocks: Create Objects and inject dependency
     */

    @Mock
    private CustomerRepo customerRepository;

    @InjectMocks
    private CustomerService customerService;

   @Before
    public void setup(){
       customerService = new CustomerService();
   }


   @Test
   public void getCustomersTest() {
       // Create a new Customer List //
       List<Customer> myCustomers = new ArrayList<>();
       Customer customer1 = new Customer("1", "salah", "khalidi","slh@email.com", new String[]{"s1","s2"});
       myCustomers.add(customer1);

       // Say to my MongoRepo to use my List //
       doReturn(myCustomers).when(customerRepository).findAll();

       // Test the methode //
       List<Customer> result = customerService.getAllCustomers();

       // Assert //
       assertThat(result).isNotEmpty();
   }


    @Test
    public void getCustomerByIdTest(){
       // Given //
       List<Customer> myCustomers = new ArrayList<>();
       Customer customer1 = new Customer("1", "salah", "khalidi","slh@email.com", new String[]{"s1","s2"});
       myCustomers.add(customer1);
       doReturn(Optional.of(customer1)).when(customerRepository).findById("1");

       // When
       ResponseEntity<Customer> responseWanted = new ResponseEntity<Customer>(customer1, HttpStatus.OK);
       ResponseEntity<Customer> responseTest = customerService.getCustomerById("1");


       // Then
        assertThat(responseTest).isEqualTo(responseWanted);
   }


}
