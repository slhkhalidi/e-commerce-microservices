package com.example.stockservice.controller;

import com.example.stockservice.dto.CheckProductResponse;
import com.example.stockservice.model.ProductModel;
import com.example.stockservice.service.ProductService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("api/v1/product")
public class ProductController {


    @Autowired
    private ProductService productService;


    /**
     * Get All Products
     * @return
     */
    @GetMapping("/")
    public ResponseEntity<List<ProductModel>> getAllProducts() {
         return productService.getAll();
    }


    /**
     * Get All Products By Categorie
     * @param categorie
     * @return
     */
    @GetMapping("/getAllByCategorie")
    public ResponseEntity<List<ProductModel>> getAllProductsByCategorie(@RequestParam String categorie) {
        return productService.getAllProductsByCategorie(categorie);
    }


    /**
     * Create a Product
     * @param productModel
     * @return
     */
    @PostMapping("/createProduct")
    public ResponseEntity<String> createProduct(@RequestBody ProductModel productModel) {
        return productService.createProduct(productModel);
    }


    /**
     * Check are the Product in Stock or not
     * http://localhost:8082/api/v1/product/getProduct?productId=1&productId=2
     */
    @GetMapping("/getProducts/{ids}")
    public ResponseEntity<List<CheckProductResponse>> isInStock(@PathVariable String ids) throws InterruptedException {
        return productService.isInStock(ids);
    }


}
