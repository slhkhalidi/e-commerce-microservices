package com.example.ordersService.model;


import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(name= "orders")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Order {

    @Id
    @SequenceGenerator(
            name= "order_sequence",
            sequenceName = "order_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "order_sequence"
    )
    private Long orderId;

    private Integer orderValue;

    private String currentPersonId;

    private Long timeStamps;

    private String orderStatus;

    @OneToMany(cascade = CascadeType.ALL)
    private List<OrderItem> orderItems;
}
