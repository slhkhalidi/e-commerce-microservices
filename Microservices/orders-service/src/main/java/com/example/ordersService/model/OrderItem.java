package com.example.ordersService.model;


import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name="orderItems")
public class OrderItem {
   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   private Long orderItemId;
   private BigDecimal priceTotal;
   private Integer quantity;
   private String productId;

}
