package com.example.stockservice.model;


import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(value = "product")
public class ProductModel {
    @Id
    private String productId;
    private String name;
    private Integer price;
    private String description;
    private String category;
    private Boolean inStock;

}
