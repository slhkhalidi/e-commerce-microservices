package com.example.stockservice.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CheckProductResponse {
    private String productId;
    private Boolean isInStock;

}
