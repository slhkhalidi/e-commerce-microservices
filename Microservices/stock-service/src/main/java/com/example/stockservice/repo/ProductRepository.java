package com.example.stockservice.repo;

import com.example.stockservice.model.ProductModel;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends MongoRepository<ProductModel, String> {

    List<ProductModel> findAllByCategory(String categorie);

   // Optional<ProductModel> findById(String productId);

   // List<ProductModel> findAllByProductId(List<String> productId);
}
