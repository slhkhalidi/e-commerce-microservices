package com.example.ordersService.controller;


import com.example.ordersService.model.Order;
import com.example.ordersService.service.OrderService;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import io.github.resilience4j.timelimiter.annotation.TimeLimiter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Controller
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
public class OrdersController {

    private final OrderService orderService;

    /**
     * Create Order
     * We use here the Circuit Breaker, because it doesn't make any sense to make calling the stock-service if we have a
     * Failure.
     * 1- Close Circuit: All is working
     * 2- Open Circuit: If 5 Request is  failure, don't allow the call in the configured duration -> 5s.
     * 3- Half Open: After the duration it will be changed to half open, here it will start to taking the request,
     *               if the requests are working it will change to close, if not it will change to open.
     */
    @PostMapping("/createOrder")
    @CircuitBreaker(name="inventory", fallbackMethod = "fallbackMethod")
    @TimeLimiter(name = "inventory")
    @Retry(name = "inventory")
    public CompletableFuture<ResponseEntity<String>> createOrder(@RequestBody Order order) {
         return CompletableFuture.supplyAsync(()-> orderService.createOrder(order));
    }

    /**
     * Fallback will run when the Request  is failing
     * @param order
     * @param ex
     * @return
     */
    public CompletableFuture<String> fallbackMethod(Order order, RuntimeException ex) {
        return CompletableFuture.supplyAsync(()-> "Oops something went wrong, please try it again !");
    }




    /**
     * Update Order
     */
     @PutMapping("/updateOrder")
    public ResponseEntity<Order> updateOrder(@RequestBody Order order) {
         return this.orderService.updateOrder(order);
     }

    /**
     * Delete Order
     */
     @DeleteMapping("/deleteOrder")
     public ResponseEntity<String> deleteOrder(@RequestParam Long orderId) {
         return this.orderService.deleteOrder(orderId);
      }

    /**
     * Get all Orders
     */
     @GetMapping("/")
    public ResponseEntity<List<Order>> getOrders() {
         return orderService.getOrders();
     }

    /**
     * Get one Order By ID
     */
   @GetMapping("/orderId")
   public ResponseEntity<Order> getOrderById(@RequestParam Long orderId) {
       return this.orderService.getOrderById(orderId);
   }


}
